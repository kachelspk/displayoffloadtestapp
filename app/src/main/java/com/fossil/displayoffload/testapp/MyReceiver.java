package com.fossil.displayoffload.testapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class MyReceiver extends BroadcastReceiver {
    private static final String TAG = "DisplayOffloadTestApp";
    private final MyReceiverListener mListener;

    MyReceiver(MyReceiverListener listener) {
        mListener = listener;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "test");

        mListener.updateUI();
    }
}

