package com.fossil.displayoffload.testapp;

import android.content.Context;
import android.util.Log;

import com.google.android.clockwork.ambient.CompoundLayoutItem;
import com.google.android.clockwork.ambient.CustomLayoutItem;
import com.google.android.clockwork.ambient.DisplayOffloadManager;
import com.google.android.clockwork.ambient.XYOffset;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

public class SampleLayoutOffloadProvider extends OffloadProvider {
    private static final String TAG = "LayoutOffloadProvider";

    @Override
    public void load(Context context) {
        super.load(context);

        DisplayOffloadManager displayOffloadManager = DisplayOffloadManager.instance();

        CustomLayoutItem sk_app = new CustomLayoutItem();
        try {
            InputStream inputStream = context.getResources().openRawResource(R.raw.sidekick_watchapp);
            int size = inputStream.available();
            byte[] bytes = new byte[size];
            inputStream.read(bytes);
            sk_app.add("WatchApp",bytes);
        } catch (IOException e) {
            e.printStackTrace();
        }


        CompoundLayoutItem groupAll = new CompoundLayoutItem(
                Arrays.asList(sk_app),
                new XYOffset(0, 0),
                null
        );

        Log.d(TAG, "Built layout.");
        displayOffloadManager.sendActivityLayout(context, groupAll,
                0, null, null);
    }
}
