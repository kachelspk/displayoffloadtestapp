package com.fossil.displayoffload.testapp;

import android.content.Context;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.wearable.activity.WearableActivity;
import android.util.Log;

public class MainActivity extends WearableActivity implements MyReceiverListener {
    private final static String EXTRA_START_DEFAULT_OFFLOAD = "START_DEFAULT_OFFLOAD";
    private static final String TAG = "DisplayOffloadTestApp";

    private MyReceiver mReceiver;
    OffloadProvider mProvider;
    private Context mContext;
    AnalogClock mAnalogClock;

    @Override
    public void onCreate(Bundle b) {
        super.onCreate(b);
        mContext = this;
        Log.d(TAG, "ENTER onCreate");

        mAnalogClock = new AnalogClock(this);
        setContentView(mAnalogClock);
        setAmbientEnabled();

        mReceiver = new MyReceiver(this);
        IntentFilter filter = new IntentFilter("offload.update");
        this.registerReceiver(mReceiver, filter);

        mProvider = new SampleLayoutOffloadProvider();

    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "ENTER onResume");
        mProvider.load(mContext);
    }

    @Override
    public void updateUI() {
    }

    @Override
    public void onEnterAmbient(Bundle ambientDetails) {
        Log.d(TAG, "ENTER Ambient");
        super.onEnterAmbient(ambientDetails);
        //mAnalogClock.setAmbient(true);
    }

    @Override
    public void onExitAmbient(){
        Log.d(TAG, "EXIT Ambient");
        super.onExitAmbient();
        //mAnalogClock.setAmbient(false);
    }
}
