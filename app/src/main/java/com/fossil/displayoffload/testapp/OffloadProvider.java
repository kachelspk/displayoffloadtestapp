package com.fossil.displayoffload.testapp;

import android.content.Context;
import android.util.Log;

import com.google.android.clockwork.ambient.DisplayOffloadManager;
import com.google.android.clockwork.ambient.IOffloadStateCallbacks;

public abstract class OffloadProvider implements IOffloadStateCallbacks {
    private final static String TAG = "OffloadProvider";

    public void load(Context context) {
        Log.i(TAG, "load");
        DisplayOffloadManager displayOffloadManager = DisplayOffloadManager.instance();
        displayOffloadManager.unregisterOffloadStateCallbacks(this);
        displayOffloadManager.registerOffloadStateCallbacks(this, null);
    }

    public void unload() {
        Log.i(TAG, "unload");
        DisplayOffloadManager displayOffloadManager = DisplayOffloadManager.instance();
        displayOffloadManager.unregisterOffloadStateCallbacks(this);
        displayOffloadManager.clearActivityLayout();
    }

    @Override
    public void onOffloadStart() {
        Log.i(TAG, "onOffloadStart");
    }

    @Override
    public void onOffloadEnd() {
        Log.i(TAG, "onOffloadEnd");
    }

    @Override
    public void onOffloadUpdate() {
        Log.i(TAG, "onOffloadUpdate");
    }
}
